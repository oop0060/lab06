package com.arukkha.lab06;

public class BookBank {
    private String name;
    private double getbalance;
    public BookBank(String name, double balance) {
        this.name = name;
        this.getbalance = balance;
    }

    public boolean deposit(double money) {
        if (money <= 0) {
            return false;
        }
        getbalance = getbalance + money;
        return true;
    }

    public boolean withdraw(double money) {
        if (money <= 0) {
            return false;

        }
        if(money>getbalance) {
            return false;
        }
        getbalance = getbalance - money;
        return true;
    }

    public void print() {
        System.out.println(name + " " + getbalance);

    }
    public String getName() {
        return name;
    }

    public double getbalance() {
        return getbalance;
    }
}
