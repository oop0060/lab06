package com.arukkha.lab06;

public class BookBankApp {
    public static void main(String[] args) {
        BookBank arukkha = new BookBank("Worawit" , 100.0);
        arukkha.print();
        arukkha.deposit(50);
        arukkha.print();
        arukkha.withdraw(50);
        arukkha.print();

        BookBank lung = new BookBank("Lung" , 1);
        lung.deposit(1000000);
        lung.withdraw(10000000);
        lung.print();

        BookBank weed = new BookBank("Weed" , 10);
        weed.deposit(10000000);
        weed.withdraw(1000000);
        weed.print();
    }
}
