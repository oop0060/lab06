package com.arukkha.lab06;

public class RobotApp {
    public static void main(String[] args) {
        Robot body = new Robot("Body", 'B', 1, 0);
        Robot petter = new Robot("Petter", 'P', 10, 15);
        body.right();
        body.print();
        petter.print();

        for (int y = Robot.Y_MIN; y <= Robot.Y_MAX; y++) {
            for (int x = Robot.X_MIN; x <= Robot.X_MAX; x++) {
                if(body.getX() == x && body.getY()==y) {
                    System.out.print(body.getSymbol());
                } else if(petter.getX() == x && body.getY()==y) {
                    System.out.print(petter.getSymbol());
                } else {
                    System.out.print("-");
                }
            }
            System.out.println();
        }
    }
}
